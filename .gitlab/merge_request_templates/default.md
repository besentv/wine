### Before sumbitting a Wine merge request, I made sure to:

- [ ] read through this guide: https://wiki.winehq.org/Submitting_Patches
- [ ] put my real name on my Gitlab profile.
- [ ] commit my patches with the same name locally, as provided on Gitlab. (See: https://wiki.winehq.org/Git_Wine_Tutorial)
- [ ] not provide any disassemled or reverse engineered parts of Windows code. This includes ReactOS code.
- [ ] follow the basic style guidelines: https://wiki.winehq.org/Wine_Developer%27s_Guide/Coding_Practice#Some_notes_about_style
- [ ] write tests for my patches, if needed.
